# **Zooming issue from double tap and pinch on mobile browsers**

| Android | iOS |
| :-----: | :-: |
| ![](android.gif) | ![](ios.gif) |

# **3 solutions for preventing zooming from double tap and pinch on mobile browsers**

### **[DEMO](https://S-Suraphat.gitlab.io/prevent-zooming-from-double-tap-and-pinch)**

## 1. HTML

```
<!-- prevent zooming from double tapping and pinching on Android -->
<meta name="viewport" content="width=device-width, height=device-height, target-densitydpi=device-dpi, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
```

## 2. CSS
```
* {
    // prevent zooming from double tapping on iOS Safari
    touch-action: manipulation;
}
```

## 3. JavaScript
```
// prevent zooming from double tapping on iOS
window.lastTouchTimestamp = 0;
document.addEventListener('touchstart', function (event) {
const nowTouchTimestamp = new Date().getTime();
const tapDelayThreshold = 300;
const tapDelay = nowTouchTimestamp - window.lastTouchTimestamp;
    if (tapDelay <= tapDelayThreshold) {
        event.preventDefault();
    }
    window.lastTouchTimestamp = nowTouchTimestamp;
}, { passive: false });
```

```
// prevent zooming from pinching on iOS
document.addEventListener('touchmove', function (event) {
    if (event.touches.length > 1) {
        event.preventDefault();
    }
}, { passive: false });
```

### **[BLOG](https://map.longdo.com/blog/?p=6448)**
